﻿Imports System.IO
Imports System.Diagnostics
Imports System.Runtime.InteropServices
Imports System.Threading

Public Class Form1
    Private PerCounter As System.Diagnostics.PerformanceCounter
    Public memoryLimit As Integer
    Public sysFunc As Boolean
    Dim config As New Configuration.ConfigXmlDocument
    Dim pathFail As Object

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try
            config.Load("config.xml")
            memoryLimit = Int32.Parse(config.DocumentElement.GetElementsByTagName("memory").Item(0).Attributes.GetNamedItem("limit").Value)
            sysFunc = Boolean.Parse(config.DocumentElement.GetElementsByTagName("sysfunc").Item(0).Attributes.GetNamedItem("flag").Value)
        Catch
            config.LoadXml("<config><memory limit='100' />")
            config.LoadXml("<sysfunc flag='true' /></config>")
            config.Save("config.xml")
            memoryLimit = 100
            sysFunc = False
        End Try
        ' MessageBox.Show("memory limit" & memoryLimit)


        PerCounter = New System.Diagnostics.PerformanceCounter
        PerCounter.CategoryName = "Processor"
        PerCounter.CounterName = "% Processor Time"
        PerCounter.InstanceName = "_Total"
        Timer1.Interval = 1000
        Timer1.Start()
        '/////////////////////////////////////////////////////////////////////////////////////////////////////////////'
        Processes()


    End Sub
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Dim i As Integer = Integer.Parse(Format(PerCounter.NextValue, "##0"))

        Dim oPerf1 As New PerformanceCounter

        oPerf1.CategoryName = "Processor"
        oPerf1.CounterName = "% Processor Time"
        oPerf1.InstanceName = "0"


        ToolStripStatusLabel2.Text = " %" & "Загрузка ЦП: " & i
        Try
            Dim Pr As Process = Process.GetProcessesByName(ListBox1.SelectedItem.ToString)(0)
            usedMemoryLabel.Text = "Потребляет: " & (Math.Round((Process.GetProcessesByName(ListBox1.SelectedItem.ToString)(0).WorkingSet64.ToString) / (1024 * 1024))) & " mb"

            If ((Now.Hour - Pr.StartTime.Hour) = 0) Then
                workingTimeLabel.Text = "Уже работает: " & Math.Abs(Now.Minute - Pr.StartTime.Minute) & " мин."
            Else
                workingTimeLabel.Text = "Уже работает: " & (Now.Hour - Pr.StartTime.Hour) & " ч : " & Math.Abs(Now.Minute - Pr.StartTime.Minute) & " мин."
            End If

            Dim fileName = Process.GetProcessesByName(ListBox1.SelectedItem.ToString)(0).ToString
            Dim file1 = fileName.Substring(fileName.LastIndexOf("(") + 1, fileName.LastIndexOf(")") - fileName.LastIndexOf("(") - 1)
            ' MessageBox.Show(file1)


            If (Integer.Parse((Math.Round((Process.GetProcessesByName(ListBox1.SelectedItem.ToString)(0).WorkingSet64.ToString) / (1024 * 1024)))) > 200) Then
                processWarningLabel.Text = "Процесс " & file1
                processWarningLabel.Text += " потребляет много ресурсов!"
            Else

                processWarningLabel.Text = ""
            End If
        Catch ex As Exception
            ex.Message.ToString()
        End Try
        ToolStripProgressBar1.Value = i

    End Sub
    Private Sub ProccesKill_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProccesKill.Click

        If ListBox1.SelectedItem = Nothing Then
            MsgBox("Выберите процесс")
        Else

            Try
                Process.GetProcessesByName(ListBox1.SelectedItem)(0).Kill() : Processes()
                MsgBox("Процесс завершен")
            Catch ex As Exception
                ex.Message.ToString()
            End Try

        End If
    End Sub
    Sub Processes()
        Dim i As Integer = Integer.Parse(Format(PerCounter.NextValue, "##0"))
        ListBox1.Items.Clear()
        ListBox2.Items.Clear()
        Dim fileName As String
        fileName = My.Computer.FileSystem.GetTempFileName()
        Dim RealProcesses() As Process = Process.GetProcesses

       

        Dim PermissedProcesses() As String = IO.File.ReadAllLines(fileName, System.Text.Encoding.GetEncoding(1251))
        Dim ExistingProcess As Boolean
        For Each RealProcess As Process In RealProcesses
            ExistingProcess = False
            For Each PermissedProcess As String In PermissedProcesses
                If RealProcess.ProcessName = PermissedProcess Then
                    ExistingProcess = True

                End If
            Next
            'Dim flag1 As Boolean = False

            Try
                
                If Form2.SystemFunctionsCheckBox.Checked = sysFunc Then
                    Dim Path As Process = Process.GetProcessesByName(RealProcess.ProcessName)(0)
                    Dim Ph As String = Path.MainModule.FileName
                    If Not (Ph.Contains("C:\Windows\SysWOW64\") Or Ph.Contains("C:\Windows\System32\")) Then
                        'flag1 = True
                        If ExistingProcess = False Then ListBox1.Items.Add(RealProcess.ProcessName) : ToolStripStatusLabel1.Text = "Процессов: " & ListBox1.Items.Count
                    End If
                Else
                    If ExistingProcess = False Then ListBox1.Items.Add(RealProcess.ProcessName) : ToolStripStatusLabel1.Text = "Процессов: " & ListBox1.Items.Count

                End If

            Catch Ex As Exception
                'MessageBox.Show(flag1)

            End Try
            ''Dim pathFile As String = Ph.Substring(0, Ph.LastIndexOf("\"))

            If ExistingProcess = False Then
                If (Math.Round(RealProcess.WorkingSet64 / (1024 * 1024)) > memoryLimit) _
                    Then ListBox2.Items.Add(RealProcess.ProcessName)
            End If



        Next

    End Sub
    Private Sub ProcessDirectory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProcessDirectory.Click
        If ListBox1.SelectedItem = Nothing Then
            MsgBox("Выберите процесс")
        Else
            Dim Path As Process = Process.GetProcessesByName(ListBox1.SelectedItem)(0)
            Try
                Dim Ph As String = Path.MainModule.FileName
                Dim pathFile As String = Ph.Substring(0, Ph.LastIndexOf("\")) 'Replace(Ph, ListBox1.SelectedItem & ".exe", "")

                Process.Start(pathFile)
            Catch Ex As Exception
                MessageBox.Show("Ошибка: " & Ex.Message)
            End Try
        End If
    End Sub
    Private Sub ProcessRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ProcessRefresh.Click
        Processes()
    End Sub


    Private Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click, ЗапуститьПроцессToolStripMenuItem.Click

        Dim myStream As Stream = Nothing
        Dim openFileDialog1 As New OpenFileDialog()
        Dim fileName As String
        Dim index As Integer
        Dim file As String
        Dim length As Integer
        Dim WaitforDone As New ManualResetEvent(False)

        openFileDialog1.InitialDirectory = "C:\"
        openFileDialog1.Filter = "exe (*.exe)|*.exe"
        openFileDialog1.FilterIndex = 2
        openFileDialog1.RestoreDirectory = True
        Try
            If openFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
                Try
                    myStream = openFileDialog1.OpenFile()
                    fileName = openFileDialog1.FileName.ToString
                    index = openFileDialog1.FileName.ToString.LastIndexOf("\") + 1
                    length = openFileDialog1.FileName.ToString.LastIndexOf(".") - index

                    file = fileName.Substring(index, length)
                    ' MessageBox.Show(Path.GetDirectoryName(file))
                    ' cool part of code for function detection
                    Dim reader As StreamReader = My.Computer.FileSystem.OpenTextFileReader(fileName)
                    Dim buffer As String = reader.ReadToEnd()

                    Dim arr() As String = {"HKEY_CURRENT_USER", "HKEY_CLASSES_ROOT", "HKEY_LOCAL_MACHINE", "HKEY_USERS", "HKEY_CURRENT_CONFIG", "TRegistry", "KEY_EXECUTE", "KEY_READ", "RegOpenKey", "RegOpenKeyEx", "RegCreateKeyEx"}
                    Dim good As Boolean = True
                    For index = 0 To arr.GetUpperBound(0)
                        If (buffer.Contains(arr(index)) = True) Then
                            'MessageBox.Show("Обнаружены подозрительные функции!")
                            good = False
                            Exit For
                        End If
                    Next
                    If (good = True) Then
                        MessageBox.Show("Программа не является зловредной!")
                    End If

                    If (myStream IsNot Nothing) Then


                        Try
                            Process.Start(openFileDialog1.FileName)
                            WaitforDone.WaitOne(2000, True)

                            If ((Process.GetProcessesByName(file)(0).WorkingSet64) > memoryLimit Or good = False) Then
                                Process.GetProcessesByName(file)(0).Kill()
                                If (good = False) Then
                                    MessageBox.Show("Были обнаружены подозрительные функции, невозможно запустить!")
                                Else
                                    MessageBox.Show("Приложение потребляет много ресурсов, ")
                                End If
                            Else

                            End If
                        Catch Ex As Exception
                            MessageBox.Show(Ex.Message)
                        End Try

                    End If
                Catch Ex As Exception
                    MessageBox.Show(Ex.Message)
                End Try
            End If

        Catch Ex As Exception
            MessageBox.Show("Невозможно запустить. Ошибка: " & Ex.Message)
        Finally
            ' Check this again, since we need to make sure we didn't throw an exception on open.
            If (myStream IsNot Nothing) Then
                myStream.Close()
            End If

        End Try



    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click, НастройкиToolStripMenuItem.Click

        If Form2.ShowDialog() = Windows.Forms.DialogResult.OK Then
            config.DocumentElement.GetElementsByTagName("memory").Item(0).Attributes.GetNamedItem("limit").Value = memoryLimit.ToString
            config.DocumentElement.GetElementsByTagName("sysfunc").Item(0).Attributes.GetNamedItem("flag").Value = sysFunc.ToString
            config.Save("config.xml")
        End If
    End Sub

    Private Sub ОПрограммеToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ОПрограммеToolStripMenuItem.Click
        MessageBox.Show("Autor: Malina Alexander, 2017", "About")
    End Sub

    Private Sub ВыходToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ВыходToolStripMenuItem.Click
        Me.Close()

    End Sub


End Class
