﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Форма переопределяет dispose для очистки списка компонентов.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Является обязательной для конструктора форм Windows Forms
    Private components As System.ComponentModel.IContainer

    'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
    'Для ее изменения используйте конструктор форм Windows Form.  
    'Не изменяйте ее в редакторе исходного кода.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ProccesKill = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProcessDirectory = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProcessRefresh = New System.Windows.Forms.ToolStripMenuItem()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.usedMemoryLabel = New System.Windows.Forms.Label()
        Me.processWarningLabel = New System.Windows.Forms.Label()
        Me.ListBox2 = New System.Windows.Forms.ListBox()
        Me.workingTimeLabel = New System.Windows.Forms.Label()
        Me.listbox2Title = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripProgressBar1 = New System.Windows.Forms.ToolStripProgressBar()
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ЗапуститьПроцессToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ВыходToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.СвойстваToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.НастройкиToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.СправкаToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ОПрограммеToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.infoPanel = New System.Windows.Forms.Panel()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.infoPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        'ListBox1
        '
        Me.ListBox1.BackColor = System.Drawing.Color.GhostWhite
        Me.ListBox1.ContextMenuStrip = Me.ContextMenuStrip1
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.Location = New System.Drawing.Point(6, 64)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(258, 472)
        Me.ListBox1.TabIndex = 0
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ProccesKill, Me.ProcessDirectory, Me.ProcessRefresh})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(250, 70)
        '
        'ProccesKill
        '
        Me.ProccesKill.Name = "ProccesKill"
        Me.ProccesKill.Size = New System.Drawing.Size(249, 22)
        Me.ProccesKill.Text = "Завершить процесс"
        '
        'ProcessDirectory
        '
        Me.ProcessDirectory.Name = "ProcessDirectory"
        Me.ProcessDirectory.Size = New System.Drawing.Size(249, 22)
        Me.ProcessDirectory.Text = "Открыть место хранения файла"
        '
        'ProcessRefresh
        '
        Me.ProcessRefresh.Name = "ProcessRefresh"
        Me.ProcessRefresh.Size = New System.Drawing.Size(249, 22)
        Me.ProcessRefresh.Text = "Обновить"
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'usedMemoryLabel
        '
        Me.usedMemoryLabel.AutoSize = True
        Me.usedMemoryLabel.Location = New System.Drawing.Point(20, 30)
        Me.usedMemoryLabel.Name = "usedMemoryLabel"
        Me.usedMemoryLabel.Size = New System.Drawing.Size(0, 13)
        Me.usedMemoryLabel.TabIndex = 3
        '
        'processWarningLabel
        '
        Me.processWarningLabel.AutoSize = True
        Me.processWarningLabel.Location = New System.Drawing.Point(20, 70)
        Me.processWarningLabel.Name = "processWarningLabel"
        Me.processWarningLabel.Size = New System.Drawing.Size(0, 13)
        Me.processWarningLabel.TabIndex = 4
        '
        'ListBox2
        '
        Me.ListBox2.BackColor = System.Drawing.Color.GhostWhite
        Me.ListBox2.FormattingEnabled = True
        Me.ListBox2.Location = New System.Drawing.Point(278, 66)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.Size = New System.Drawing.Size(494, 212)
        Me.ListBox2.TabIndex = 6
        '
        'workingTimeLabel
        '
        Me.workingTimeLabel.AutoSize = True
        Me.workingTimeLabel.Location = New System.Drawing.Point(20, 110)
        Me.workingTimeLabel.Name = "workingTimeLabel"
        Me.workingTimeLabel.Size = New System.Drawing.Size(0, 13)
        Me.workingTimeLabel.TabIndex = 8
        '
        'listbox2Title
        '
        Me.listbox2Title.Location = New System.Drawing.Point(275, 50)
        Me.listbox2Title.Name = "listbox2Title"
        Me.listbox2Title.Size = New System.Drawing.Size(276, 13)
        Me.listbox2Title.TabIndex = 9
        Me.listbox2Title.Text = "Приложения, которые рекомендуется закрыть"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(669, 499)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(115, 37)
        Me.Button1.TabIndex = 10
        Me.Button1.Text = "Запустить приложение"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1, Me.ToolStripProgressBar1, Me.ToolStripStatusLabel2})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 539)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional
        Me.StatusStrip1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.StatusStrip1.Size = New System.Drawing.Size(784, 22)
        Me.StatusStrip1.SizingGrip = False
        Me.StatusStrip1.TabIndex = 11
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(120, 17)
        Me.ToolStripStatusLabel1.Text = "ToolStripStatusLabel1"
        '
        'ToolStripProgressBar1
        '
        Me.ToolStripProgressBar1.Name = "ToolStripProgressBar1"
        Me.ToolStripProgressBar1.Size = New System.Drawing.Size(100, 16)
        Me.ToolStripProgressBar1.Step = 1
        Me.ToolStripProgressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(120, 17)
        Me.ToolStripStatusLabel2.Text = "ToolStripStatusLabel2"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(278, 499)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(116, 37)
        Me.Button2.TabIndex = 12
        Me.Button2.Text = "Настройки"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.СвойстваToolStripMenuItem, Me.СправкаToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(784, 24)
        Me.MenuStrip1.TabIndex = 13
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ЗапуститьПроцессToolStripMenuItem, Me.ВыходToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(48, 20)
        Me.FileToolStripMenuItem.Text = "Файл"
        '
        'ЗапуститьПроцессToolStripMenuItem
        '
        Me.ЗапуститьПроцессToolStripMenuItem.Name = "ЗапуститьПроцессToolStripMenuItem"
        Me.ЗапуститьПроцессToolStripMenuItem.Size = New System.Drawing.Size(178, 22)
        Me.ЗапуститьПроцессToolStripMenuItem.Text = "Запустить процесс"
        '
        'ВыходToolStripMenuItem
        '
        Me.ВыходToolStripMenuItem.Name = "ВыходToolStripMenuItem"
        Me.ВыходToolStripMenuItem.Size = New System.Drawing.Size(178, 22)
        Me.ВыходToolStripMenuItem.Text = "Выход"
        '
        'СвойстваToolStripMenuItem
        '
        Me.СвойстваToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.НастройкиToolStripMenuItem})
        Me.СвойстваToolStripMenuItem.Name = "СвойстваToolStripMenuItem"
        Me.СвойстваToolStripMenuItem.Size = New System.Drawing.Size(70, 20)
        Me.СвойстваToolStripMenuItem.Text = "Свойства"
        '
        'НастройкиToolStripMenuItem
        '
        Me.НастройкиToolStripMenuItem.Name = "НастройкиToolStripMenuItem"
        Me.НастройкиToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me.НастройкиToolStripMenuItem.Text = "Настройки"
        '
        'СправкаToolStripMenuItem
        '
        Me.СправкаToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ОПрограммеToolStripMenuItem})
        Me.СправкаToolStripMenuItem.Name = "СправкаToolStripMenuItem"
        Me.СправкаToolStripMenuItem.Size = New System.Drawing.Size(65, 20)
        Me.СправкаToolStripMenuItem.Text = "Справка"
        '
        'ОПрограммеToolStripMenuItem
        '
        Me.ОПрограммеToolStripMenuItem.Name = "ОПрограммеToolStripMenuItem"
        Me.ОПрограммеToolStripMenuItem.Size = New System.Drawing.Size(149, 22)
        Me.ОПрограммеToolStripMenuItem.Text = "О программе"
        '
        'infoPanel
        '
        Me.infoPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.infoPanel.Controls.Add(Me.workingTimeLabel)
        Me.infoPanel.Controls.Add(Me.processWarningLabel)
        Me.infoPanel.Controls.Add(Me.usedMemoryLabel)
        Me.infoPanel.Location = New System.Drawing.Point(278, 293)
        Me.infoPanel.Name = "infoPanel"
        Me.infoPanel.Size = New System.Drawing.Size(494, 205)
        Me.infoPanel.TabIndex = 14
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.ClientSize = New System.Drawing.Size(784, 561)
        Me.ContextMenuStrip = Me.ContextMenuStrip1
        Me.Controls.Add(Me.listbox2Title)
        Me.Controls.Add(Me.infoPanel)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ListBox2)
        Me.Controls.Add(Me.ListBox1)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Location = New System.Drawing.Point(100, 100)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.MaximizeBox = False
        Me.Name = "Form1"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Text = "Диспетчер задач"
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.infoPanel.ResumeLayout(False)
        Me.infoPanel.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ProccesKill As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProcessDirectory As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ProcessRefresh As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents usedMemoryLabel As System.Windows.Forms.Label
    Friend WithEvents processWarningLabel As System.Windows.Forms.Label
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents workingTimeLabel As System.Windows.Forms.Label
    Friend WithEvents listbox2Title As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents FileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ЗапуститьПроцессToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ВыходToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents СвойстваToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents НастройкиToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents СправкаToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ОПрограммеToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents infoPanel As Panel
    Friend WithEvents ToolStripProgressBar1 As System.Windows.Forms.ToolStripProgressBar
End Class
